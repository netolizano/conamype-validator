﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using Microsoft.Extensions.Configuration;
using conamype_validator.Models;
using conamype_validator.Services;
using conamype_validator.Helpers;
using Newtonsoft.Json;
using System.Text;

namespace conamype_validator.Controllers
{
    [Route("api/[controller]")]
    public class MypeController : Controller
    {
        IConfiguration configuration;
        public MypeController(IConfiguration iconfiguration)
        {
            this.configuration = iconfiguration;
        }
        [HttpPost]
        public async Task<HttpResponseMessage> PostAsync([FromBody]MypeRegister register)
        {
            DuiService duiService = new DuiService();
            CertificateService certService = new CertificateService();
            DuiRequest dRequest = new DuiRequest();
            Validation makeValidation = new Validation();
            ValidatorResponse validatorResponse = new ValidatorResponse();
            CertResponse certResponse = new CertResponse();
            var response = new HttpResponseMessage();
            HttpRequestMessage request = new HttpRequestMessage(); ;
            dRequest.codiApli = configuration.GetSection("DuiApliCode").Value;
            dRequest.direIP = configuration.GetSection("DuiIP").Value;
            dRequest.nombFunc = configuration.GetSection("DuiFuncName").Value;
            dRequest.nombUsua = configuration.GetSection("DuiUser").Value;
            dRequest.numTram = "minec0001"; //make changes when implement logs.
            string url = configuration.GetSection("DuiURL").Value;
            dRequest.tokn = configuration.GetSection("DuiToken").Value;
            DuiData dData = new DuiData();
            dData.value = register.dui;
            dRequest.filt = new List<DuiData>();
            dRequest.filt.Add(dData);
            try
            {
                var resultDuiService = await duiService.validateDuiAsync(dRequest, url);
                validatorResponse = makeValidation.validateDui(resultDuiService, register);
                if (validatorResponse.isValid && makeValidation.validateNit(register))
                {
                    MypeCertRequest certificateRequest = new MypeCertRequest();
                    certificateRequest.businessName = register.businessName;
                    certificateRequest.economicActivities = register.economicActivity;
                    certificateRequest.mypeExpireDate = register.mypeExpireDate;
                    certificateRequest.mypeIssueDate = register.mypeIssueDate;
                    certificateRequest.nit = register.nit;
                    certificateRequest.owner = register.owner;
                    certificateRequest.qrUrl = configuration.GetSection("BaseQrUrl").Value + register.nit;
                    //call certificate
                    certResponse = await certService.callCertCreatorAsync(certificateRequest, configuration.GetSection("URLCertGenerator").Value);
                    if (certResponse != null && certResponse.isValid)
                    {
                        
                        response.StatusCode = HttpStatusCode.OK;
                        response.Content = new StreamContent(certResponse.cert);
                        response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                    }
                    else
                    {
                        validatorResponse.isValid = false;
                        validatorResponse.message = certResponse.message;
                        response.StatusCode = HttpStatusCode.BadRequest;
                        response.Content = new StringContent(JsonConvert.SerializeObject(validatorResponse), Encoding.UTF8, "application/json");
                    }
                }
                else
                {
                    response.StatusCode = HttpStatusCode.BadRequest;
                    validatorResponse.isValid = false;
                    validatorResponse.message = "DUI or NIT not found";
                    response.Content = new StringContent(JsonConvert.SerializeObject(validatorResponse), Encoding.UTF8, "application/json");
                }
               
            }
            catch (Exception e)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                validatorResponse.isValid = false;
                validatorResponse.message = e.Message;
                response.Content = new StringContent(JsonConvert.SerializeObject(validatorResponse), Encoding.UTF8, "application/json");
            }
            return response;
        }
    }
}
