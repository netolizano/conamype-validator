﻿using conamype_validator.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace conamype_validator.Services
{
    public class DuiService
    {
        public async Task<RnpnResponse> validateDuiAsync(DuiRequest duiRequest, string url)
        {
            RnpnResponse result = new RnpnResponse();
            string content = JsonConvert.SerializeObject(duiRequest);
            var contentData = new StringContent(content, System.Text.Encoding.UTF8, "application/json");
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));
                try
                {
                    var response = await httpClient.PostAsync(url, contentData).ConfigureAwait(false);
                    if (response.IsSuccessStatusCode)
                    {
                        var xml = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);

                        if (xml != null)
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(RnpnResponse));
                            result = await Task.Run(() =>
                            {
                                return (RnpnResponse)serializer.Deserialize(xml);
                            }).ConfigureAwait(false);
                        }
                        else
                        {
                            result.resp = false;
                            result.message = "can't get DUI information";
                        }
                    }
                    else
                    {
                        result.resp = false;
                        result.message = "bad response from rnpn service";
                    }
                }
                catch (Exception)
                {

                    throw;
                }
                
                return result;
            }
        }
    }
}
