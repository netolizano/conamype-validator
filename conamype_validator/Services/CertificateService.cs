﻿using conamype_validator.Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace conamype_validator.Services
{
    public class CertificateService
    {
        public async Task<CertResponse> callCertCreatorAsync(MypeCertRequest mypeCertRequest, string urlCertGenerator)
        {
            CertResponse result = new CertResponse();
            string content = JsonConvert.SerializeObject(mypeCertRequest);
            var contentData = new StringContent(content, Encoding.UTF8, "application/json");
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/pdf"));
                try
                {
                    var response = await httpClient.PostAsync(urlCertGenerator, contentData).ConfigureAwait(false);

                    if (response.IsSuccessStatusCode)
                    {
                        var cert = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);

                        if (cert != null)
                        {
                            result.cert = cert;
                            result.isValid = true;
                            result.message = "OK";
                        }
                        else
                        {
                            result.isValid = false;
                            result.message = "Cert -> null";
                        }
                    }
                    else
                    {
                        result.isValid = false;
                        result.message = "Error from cert generator ";
                    }
                }
                catch (System.Exception)
                {
                    throw;
                }
               
            }
            return result;
        }
    }
}
