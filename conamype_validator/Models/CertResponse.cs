﻿using System.IO;

namespace conamype_validator.Models
{
    public class CertResponse
    {
        public bool isValid { get; set; }

        public string message { get; set; }

        public Stream cert { get; set; }
    }
}
