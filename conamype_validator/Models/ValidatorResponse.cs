﻿namespace conamype_validator.Models
{
    public class ValidatorResponse
    {
        public bool isValid { get; set; }

        public string message { get; set; }
    }
}
