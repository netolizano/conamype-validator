﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace conamype_validator.Models
{
    [Serializable]
    [XmlRoot(ElementName = "response", Namespace = "http://api.rnpn.gob.sv/producer")]
    public class RnpnResponse
    {
        [XmlAttribute("resp", Namespace = "http://api.rnpn.gob.sv/producer")]

        public bool resp { get; set; }


        [XmlElement("data", Type = typeof(Person), Namespace = "http://api.rnpn.gob.sv/producer")]
        public List<Person> data { get; set; }

        [XmlIgnore]
        public string message { get; set; }
    }
}
