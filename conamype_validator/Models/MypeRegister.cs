﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace conamype_validator.Models
{
    public class MypeRegister
    {
        public string nit { get; set; }
        public string businessName { get; set; }
        public string owner { get; set; }
        public string dui { get; set; }
        public DateTime birthday { get; set; }
        public string economicActivity { get; set; }
        public DateTime mypeIssueDate { get; set; }
        public DateTime mypeExpireDate { get; set; }

    }
}
