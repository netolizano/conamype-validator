﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace conamype_validator.Models
{
    [XmlRoot(ElementName = "data", Namespace = "http://api.rnpn.gob.sv/producer")]
    public class Person
    {
        /*
         "apellido2": "BELTRAN",
            "fechNaci": "01/09/1995",
            "nombre2": "TEODORO",
            "dui": "04541804-4",
            "nombre1": "JACOBO",
            "apellido1": "TORRES",
            "fechVenc": "16/11/2018"

         */
        [XmlElement(ElementName = "apellido2", Namespace = "http://api.rnpn.gob.sv/producer")]
        public string otherLastNames { get; set; }
        [XmlElement(ElementName = "fechNaci", Namespace = "http://api.rnpn.gob.sv/producer")]
        public string birthday { get; set; }
        [XmlElement(ElementName = "nombre2", Namespace = "http://api.rnpn.gob.sv/producer")]
        public string otherNames { get; set; }
        [XmlElement(ElementName = "dui", Namespace = "http://api.rnpn.gob.sv/producer")]
        public string dui { get; set; }
        [XmlElement(ElementName = "apellido1", Namespace = "http://api.rnpn.gob.sv/producer")]
        public string lastname { get; set; }
        [XmlElement(ElementName = "nombre1", Namespace = "http://api.rnpn.gob.sv/producer")]
        public string name { get; set; }
        [XmlElement(ElementName = "fechVenc", Namespace = "http://api.rnpn.gob.sv/producer")]
        public string expireDate { get; set; }


    }
}
