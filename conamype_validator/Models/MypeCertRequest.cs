﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace conamype_validator.Models
{
    public class MypeCertRequest
    {
        public string businessName { get; set; }
        public string owner { get; set; }
        public string nit { get; set; }

        public string economicActivities { get; set; }

        public DateTime mypeIssueDate { get; set; }

        public DateTime mypeExpireDate { get; set; }

        public string qrUrl { get; set; }
    }
}
