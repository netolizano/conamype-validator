﻿using System.Collections.Generic;

namespace conamype_validator.Models
{
    public class DuiRequest
    {
        public string codiApli { get; set; }

        public string nombUsua { get; set; }

        public string direIP { get; set; }

        public string tokn { get; set; }

        public string numTram { get; set; }

        public string nombFunc { get; set; }

        public List<DuiData> filt { get; set; }
    }
}
