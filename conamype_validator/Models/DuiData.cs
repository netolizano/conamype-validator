﻿namespace conamype_validator.Models
{
    public class DuiData
    {
        public string name { get { return "dui"; } }

        public string value { get; set; }
    }
}
