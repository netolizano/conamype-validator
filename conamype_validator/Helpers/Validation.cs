﻿using conamype_validator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace conamype_validator.Helpers
{
    public class Validation
    {
        public ValidatorResponse validateDui(RnpnResponse rnpnResponse, MypeRegister mypeRegister)
        {
            ValidatorResponse vr = new ValidatorResponse();
            vr.isValid = false;
            if (rnpnResponse.data != null)
            {
                Person personDui = rnpnResponse.data.First();

                if (string.Compare(personDui.dui, mypeRegister.dui) == 0 && DateTime.Compare(Convert.ToDateTime(personDui.birthday), mypeRegister.birthday) == 0)
                {
                    string name1Dui = (string.IsNullOrWhiteSpace(personDui.name) == true ? "" : personDui.name);
                    string otherNamesDui = (string.IsNullOrWhiteSpace(personDui.otherNames) == true ? "" : personDui.otherNames);
                    string lastnameDui = (string.IsNullOrWhiteSpace(personDui.lastname) == true ? "" : personDui.lastname);
                    string otherLastnamesDui = (string.IsNullOrWhiteSpace(personDui.otherLastNames) == true ? "" : personDui.otherLastNames);

                    string fullNameMype = mypeRegister.owner;
                    string fullNameDui = name1Dui + " " + otherNamesDui + " " + lastnameDui + " " + otherLastnamesDui;

                    string[] namesMype = fullNameMype.Split(' ');
                    string[] namesDui = fullNameDui.Split(' ');
                    string fullNameMypeP = "";
                    string fullNameDuiP = "";
                    for (int i = 0; i < namesMype.Length; i++)
                    {
                        fullNameMypeP += namesMype[i] + "|";
                    }
                    if (namesMype.Length == 2)
                    {
                        //conamype or miempresa only has name1 and lastname
                        //make validation againts only for firstname and lastname
                        fullNameDuiP = name1Dui + "|" + lastnameDui + "|";
                    }
                    else
                    {
                        for (int i = 0; i < namesDui.Length; i++)
                        {
                            fullNameDuiP += namesDui[i] + "|";
                        }
                    }
                    if (string.Compare(fullNameDuiP, fullNameMypeP) == 0)
                    {
                        vr.isValid = true;
                    }
                    else
                    {
                        vr.message = "The name is different";
                        vr.isValid = false;
                    }
                }
                else
                {
                    vr.isValid = false;
                    vr.message = "DUI or birthday is not the same";
                }
            }
            else
            {
                vr.message = "DUI not found";
            }
            return vr;
        }

        public bool validateNit(MypeRegister mypeRegister)
        {
            //need to prepare the specification
            return true;
        }

        public bool validateDuiNumber(string dui)
        {
            try
            {
                int suma = 0;
                int cont = 7;
                int numeroDui = int.Parse(dui.Substring(0, 8));
                string digito = dui.Substring(8, 1);
                string chNumeroDui;
                int tmpNmb;
                int resto;
                string dgtvrf;

                numeroDui += 100000000;
                chNumeroDui = numeroDui.ToString().Trim().Substring(1, 8);

                while (cont > 0)
                {
                    tmpNmb = int.Parse(chNumeroDui.Substring(cont, 1));
                    suma += (tmpNmb * (7 - cont + 2));
                    cont--;

                }

                resto = 10 - (suma % 10);

                if (resto == 10)
                {
                    dgtvrf = "0";
                }

                else
                {
                    dgtvrf = Convert.ToString(resto);
                }


                if (dgtvrf.Equals(digito))
                {
                    return true;
                }

                else
                {
                    return false;
                }

            }

            catch (Exception)
            {

                throw;
            }

        }

        public bool validateNitNumber(string nitNumber)
        {
            if (nitNumber.Length == 14)
            {

                string nit = nitNumber;
                string ls_valor = nit.Substring(0, 13);
                string ls_posi14 = nit.Substring(13, 1);
                int li_sum = 0;
                int li_factor;
                int li_pos;
                int li_residuo;
                int li_digver;


                if (Convert.ToInt32(ls_valor.Substring(10, 3)) <= 100)
                {
                    for (li_pos = 1; li_pos <= 13; li_pos++)
                    {
                        li_sum = li_sum + Convert.ToInt32(ls_valor.Substring(li_pos - 1, 1)) * (15 - li_pos);
                    }

                    li_digver = li_sum % 11;

                    if (li_digver == 10)
                    {
                        li_digver = 0;
                    }
                }
                else
                {
                    for (li_pos = 1; li_pos <= 13; li_pos++)
                    {
                        li_factor = (3 + 6 * Convert.ToInt32((li_pos + 4) / 6)) - li_pos;
                        li_sum = li_sum + Convert.ToInt32(ls_valor.Substring(li_pos - 1, 1)) * li_factor;
                    }
                    li_residuo = li_sum % 11;

                    if (li_residuo > 1)
                    {
                        li_digver = 11 - li_residuo;
                    }
                    else
                    {
                        li_digver = 0;
                    }
                }

                if (ls_posi14 == Convert.ToString(li_digver))
                {
                    return true;
                }

            }
            return false;
        }
    }
}
